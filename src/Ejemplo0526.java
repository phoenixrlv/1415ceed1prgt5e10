/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0526.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0526 {

  public static void main(String[] args) {
    char v1[] = {'a', 'b', 'c'};
    char v2[] = {'c', 'd', 'e'};
    String s = new String(v1);
    System.out.println(s);
    System.out.println(new String(v2));
  }
}
/* EJECUCION:
 abc
 cde
 */
