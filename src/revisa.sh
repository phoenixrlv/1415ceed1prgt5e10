
# Bash Script 
# Por Paco Aldarias.


rm *.java

cp ~/Dropbox/ceed1314/apuntes/NetBeansProjects/t7/prgt7e10aldarias/src/prgt7e10aldarias/* .

echo "Renombrando ficheros con Ejemplo05 por Ejemplo07"
rename -v 's/Ejemplo07/Ejemplo05/' *.java

for i in $(ls *.java);do 
 echo $i
 
 echo "Borrando lineas con el texto package"
 cat $i | grep  -Ev package > t$i 
 mv t$i $i
 
 echo "Reemplazando el texto Ejemplo07 por Ejemplo05"
 cat $i | sed 's/Ejemplo07/Ejemplo05/g' > t$i
 mv  t$i $i
done   
