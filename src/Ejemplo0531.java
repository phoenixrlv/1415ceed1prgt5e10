/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0531.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0531 {

  static void mostrar1(String... argumentos) {
    for (String str : argumentos) {
      System.out.print(str + " ");
    }
    System.out.println("");
  }

  static void mostrar2(String[] argumentos) {
    for (String str : argumentos) {
      System.out.print(str + " ");
    }
    System.out.println("");
  }

  public static void main(String[] args) {
    mostrar1("aaa aaa", "bbb bbb", "ccc ccc");
    mostrar2(new String[]{"aaa aaa", "bbb bbb", "ccc ccc"});
  }
}
/* EJECUCION:
 aaa aaa bbb bbb ccc ccc 
 aaa aaa bbb bbb ccc ccc 
 */
