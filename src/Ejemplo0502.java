
/**
 * Fichero: Ejemplo0502.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-ene-2014
 */
public class Ejemplo0502 {

  public static void main(String args[]) {
    int v[] = {3, 4, 6, 7, 9};
    for (int i = 0; i < 5; i++) {
      System.out.println(v[i]);
    }
  }
}

/* EJECUCION:
 3
 4
 6
 7
 9
 */
