
import java.util.HashSet;
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Fichero: Ejemplo0547HashSet.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 03-dic-2014
 */
public class Ejemplo0547HashSet {

  public static void imprimir(HashSet hs) {
    Iterator it = hs.iterator();
    while (it.hasNext()) {
      System.out.println(it.next());
    }

  }

  public static void main(String[] args) {
    // TODO code application logic here

    HashSet hs = new HashSet();

    hs.add("Pepe");
    hs.add(1);
    hs.add(34.3);
    hs.add("Paco");
    hs.add("Paco");
    hs.add("Juan");

    if (hs.contains("Paco")) {
      System.out.println("Paco Existe");
    } else {
      hs.add("Paco");
    }

    hs.remove("Juan");
    imprimir(hs);

  }

}
/* EJECUCION:
 Paco Existe
 Pepe
 Paco
 */
