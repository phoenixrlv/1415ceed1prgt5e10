/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0517.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0517 {

  public static void main(String args[]) {
    String str1 = "ab";
    String str2 = "ab";
    System.out.println(str2.compareTo("aa"));
    System.out.println(str2.compareTo("ac"));
    if (str1.compareTo(str2) == 0) {
      System.out.println("Son iguales " + str1 + " y " + str2);
    }
  }
}
/* EJECUCION:
 1
 -1
 Son iguales ab y ab
 */
