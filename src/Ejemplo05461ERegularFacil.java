/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: Ejemplo0546.java
 *
 * @date 27-ene-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejemplo05461ERegularFacil {

    public static void main(String[] args) {
        Pattern p = Pattern.compile("[0-9]");
        Matcher m = p.matcher("12");
        System.out.println(m.matches());
    }
}
