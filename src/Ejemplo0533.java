/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.StringTokenizer;

/**
 * Fichero: Ejemplo0533.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0533 {

  public static void main(String args[]) {
    int numeros = 0;
    StringTokenizer stringTokenizer = new StringTokenizer("AB CD EF");
    System.out.println("Num de Palabras: " + stringTokenizer.countTokens());
    while (stringTokenizer.hasMoreTokens()) {
      System.out.println(stringTokenizer.nextToken());
    }
  }
}
/* EJECUCION:
 Num de Palabras: 3
 AB
 CD
 EF
 */
