/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0523.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0523 {

  String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

// Devuelve un numero entre 0 y la longuitud de letras
  public int calcular() {
    return (int) (Math.random() * letras.length());
  }

// Muestra una letras aleatoria de letras
  public void mostrar() {
    int indice = calcular();
    char letra = letras.charAt(indice);
    System.out.println(letra);
  }

  public static void main(String[] args) {
    new Ejemplo0523().mostrar();
  }
}
/* EJECUCION:
X
*/
