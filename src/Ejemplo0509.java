/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0509.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0509 {

  public static int busquedalineal(int valor, int[] vector) {
    int posicion = 0;
    while (posicion < vector.length) {
      if (vector[posicion] == valor) {
        return posicion;  // Encontrado
      }
      posicion++;
    } // while
    return -1; // No encontrado
  }

  public static int busquedabinaria(int valor, int[] vector) {
    int inicio = 0;
    int posicion;
    int fin = vector.length - 1;
    while (inicio <= fin) {
      posicion = (inicio + fin) / 2;
      if (vector[posicion] == valor) {
        return posicion;  // Encontrado
      } else if (valor > vector[posicion]) {
        inicio = posicion + 1;
      } else {
        fin = posicion - 1;
      }
    } // while
    return -1; // No encontrado
  }

  public static void muestra(int vector[]) {
    for (int i = 0; i < vector.length; i++) {
      System.out.print(vector[i] + " ");
    }
    System.out.println("");
  }

  public static void main(String args[]) {
    int vector[] = {1, 5, 6, 10, 12};
    int valor = 6;
    int encontrar;
    muestra(vector);

    encontrar = busquedalineal(6, vector); // Busqueda Lineal
    if (encontrar >= 0) {
      System.out.println("El valor " + valor + " se encontro en " + encontrar);
    } else {
      System.out.println("El valor " + valor + " no se encontro");
    }

    encontrar = busquedabinaria(6, vector); // Busqueda Binaria
    if (encontrar >= 0) {
      System.out.println("El valor " + valor + " se encontro en " + encontrar);
    } else {
      System.out.println("El valor " + valor + " no se encontro");
    }

  }
}
/* EJECUCION:
 1 5 6 10 12 
 El valor 6 se encontro en 2
 El valor 6 se encontro en 2
 */
