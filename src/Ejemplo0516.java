/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0516.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0516 {

  public static void main(String args[]) {
    String str1 = "PRUEBA1";
    String str2 = "PRUEBA1";
    System.out.println(str1 + " " + str2);
    System.out.println(str1.toString() + " " + str2.toString());
  }
}
/* EJECUCION:
 PRUEBA1 PRUEBA1
 PRUEBA1 PRUEBA1
 */
