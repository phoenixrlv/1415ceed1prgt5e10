/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: Ejemplo0544.java
 *
 * @date 27-ene-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejemplo0544 {

    public static void main(String[] args) throws Exception {

         // Metodo 1.      
        Pattern p = Pattern.compile("[0-9]{8}");
        Matcher m = p.matcher("12345678");     
        if (m.matches()) {
            System.out.println("La entrada se ajusta al patrón");
        } else {
            System.out.println("La entrada no se ajusta al patrón");
        }
        
        // Metodo 2. Corta
        if ( p.matcher("12345678").matches()) {
            System.out.println("La entrada se ajusta al patrón");
        } else {
            System.out.println("La entrada no se ajusta al patrón");
        }

        
    }
}
    

