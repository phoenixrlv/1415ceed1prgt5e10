/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Enumeration;
import java.util.Vector;

/**
 * Fichero: Ejemplo0539Enumeration.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 09-ene-2014
 */
public class Ejemplo0539Enumeration {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here

    Vector v = new Vector();
    v.addElement("uno");
    v.addElement("dos");
    v.addElement("cuatro");
    v.addElement("cinco");
    v.addElement("seis");
    v.addElement("siete");
    v.addElement("ocho");
    v.addElement("nueve");
    v.addElement("diez");
    v.addElement("once");
    v.addElement("doce");
    v.insertElementAt("tres", 2);
    System.out.println("n. de elementos " + v.size());
    System.out.println("dimension " + v.capacity());

    Enumeration n = v.elements();
    while (n.hasMoreElements()) {
      System.out.print(n.nextElement() + "\t");
    }
    System.out.println();
    if (v.contains("tres")) {
      System.out.println("Encontrado tres");
    }
    v.removeElement("tres");
    for (int i = 0; i < v.size(); i++) {
      System.out.print(v.elementAt(i) + "\t");
    }
    System.out.println();

    try {
//espera la pulsacion de una tecla y luego RETORNO
      System.in.read();
    } catch (Exception e) {
    }


  }
}
